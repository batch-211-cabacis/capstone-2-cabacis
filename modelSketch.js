/*

Project Name: Galeria by Angelique

Type: E Commerce API

Regular User Credentials:

email: "user@mail.com"
password: "user1234"

Admin Credentials:

email: "admin@mail.com"
password: "admin1234"

Features:
User registration
User authentication
Set user as admin (Admin only)
Retrieve all active products
Retrieve single product
Retrieve all products
Create Product (Admin only)
Update Product information (Admin only)
Archive Product (Admin only)
Make Product Available (Admin only)
Non-admin User checkout (Create Order)
Retrieve authenticated user’s orders
Retrieve all orders (Admin only)
Retrieve User Details

Other Features:
Add to Cart
	-Added Products
	-Change product quantities
	-Remove products from cart
	-Subtotal for each item
	-Total price for all items
*/

/*

DATA MODEL

3-Model Structure

User
firstName - string,
lastName - string,
email - string,
password - string,
mobileNo - string,
isAdmin - boolean,
	      default: false


Product
name - string,
description - string,
price - number
isActive - boolean
		   default: true,
createdOn - date
			default: new Date()

Order
totalAmount - number,
purchasedOn - date
		     default: new Date(),
userId - string
products - [

	{
		productId - string,
		quantity - number
	}
]

*/